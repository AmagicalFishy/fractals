Run any of these scripts w/ R. Necessary packages are:
- ggplot2
- dplyr
- magrittr
- data.table

(Any R installation worth its salt should have these packages anyway!)
